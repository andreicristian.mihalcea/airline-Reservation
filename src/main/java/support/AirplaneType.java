package support;

import lombok.Data;


public enum AirplaneType {

    A300(200,6),
    A310(169,4),
    BOEING747(350,165)
    ;


    private int numberOfSeats;

    private int hoursFlying;

    public void decrementNumberOfSeats(){
        numberOfSeats = getNumberOfSeats();
        setNumberOfSeats(numberOfSeats--);
    }
    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public int getHoursFlying() {
        return hoursFlying;
    }

    public void setHoursFlying(int hoursFlying) {
        this.hoursFlying = hoursFlying;
    }

    AirplaneType(int numberOfSeats, int hoursFlying) {
        this.numberOfSeats = numberOfSeats;
        this.hoursFlying = hoursFlying;
    }


    public void seatsDecrease(){
        System.out.println(numberOfSeats + " before decrease");
        numberOfSeats--;
        System.out.println(numberOfSeats + " seats available");
    }
}
