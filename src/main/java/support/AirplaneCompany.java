package support;



public enum AirplaneCompany {
    TAROM("TaRom",5,"OTP Bucharest"),
    WIZZAIR("WizzAir",3,"IS IASI"),
    BLUEAIR("BlueAir",4,"Cluj City"),

    ;
    private String name;

    private int numberOfAirplanes;

    private String address;

    AirplaneCompany(String name, int numberOfAirplanes, String address) {
        this.name = name;
        this.numberOfAirplanes = numberOfAirplanes;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfAirplanes() {
        return numberOfAirplanes;
    }

    public void setNumberOfAirplanes(int numberOfAirplanes) {
        this.numberOfAirplanes = numberOfAirplanes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
