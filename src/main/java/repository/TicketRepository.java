package repository;

import config.SessionTransaction;
import entity.Customer;
import entity.Flight;
import entity.Ticket;
import org.hibernate.query.Query;

import java.util.List;

public class TicketRepository extends SessionTransaction{
    public void createTicket(Ticket ticket, Flight flight) {
        openSession();
         if (flight.getNumberOfSeats() >0) {
             session.save(ticket);
             System.out.println("Ticket saved with success");
         }
        System.out.println("You can't book this ticket, flight not available");
        closeSession();
    }

    public void deleteTicket(Ticket ticket) {
        openSession();
        session.delete(ticket);
        System.out.println("Ticket deleted with success");
        closeSession();
    }

    public void updateTicket(Ticket ticket){
        openSession();
        session.update(ticket);
        System.out.println("Ticket updated with success");
        closeSession();
    }

    public List<Customer> findAirportById(int id) {
        openSession();
        Query query = session.createQuery("FROM Airport where id = '" + id + "'");
        List<Customer> result = query.list();
        closeSession();
        return result;
    }
}
