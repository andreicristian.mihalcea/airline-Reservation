package repository;

import config.SessionTransaction;
import entity.Customer;
import entity.DepartureDetails;
import org.hibernate.query.Query;

import java.util.List;

public class DepartureDetailsRepository extends SessionTransaction {
    public void createDepartureDetails(DepartureDetails departureDetails) {
        openSession();
        session.save(departureDetails);
        System.out.println("DepartureDetails saved with success");
        closeSession();
    }

    public void deleteDepartureDetails(DepartureDetails departureDetails) {
        openSession();
        session.delete(departureDetails);
        System.out.println("DepartureDetails deleted with success");
        closeSession();
    }

    public void updateDepartureDetails(DepartureDetails departureDetails){
        openSession();
        session.update(departureDetails);
        System.out.println("DepartureDetails updated with success");
        closeSession();
    }

    public List<DepartureDetails> findDepartureDetailsById(int id) {
        openSession();
        Query query = session.createQuery("FROM DepartureDetails where id = '" + id + "'");
        List<DepartureDetails> result = query.list();
        closeSession();
        return result;
    }
}
