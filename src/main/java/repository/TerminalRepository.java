package repository;

import config.SessionTransaction;
import entity.Customer;
import entity.Terminal;
import org.hibernate.query.Query;

import java.util.List;

public class TerminalRepository extends SessionTransaction {
    public void createTerminal(Terminal terminal) {
        openSession();
        session.save(terminal);
        System.out.println("Terminal saved with success");
        closeSession();
    }

    public void deleteTerminal(Terminal terminal) {
        openSession();
        session.delete(terminal);
        System.out.println("Terminal deleted with success");
        closeSession();
    }

    public void updateTerminal(Terminal terminal){
        openSession();
        session.update(terminal);
        System.out.println("Terminal updated with success");
        closeSession();
    }

    public List<Customer> findTerminalById(int id) {
        openSession();
        Query query = session.createQuery("FROM Terminal where id = '" + id + "'");
        List<Customer> result = query.list();
        closeSession();
        return result;
    }
}
