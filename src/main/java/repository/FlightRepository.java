package repository;

import config.SessionTransaction;
import entity.Customer;
import entity.Flight;
import entity.Terminal;
import org.hibernate.query.Query;

import java.util.List;

public class FlightRepository extends SessionTransaction {
    public void createFlight(Flight flight, Terminal terminal) {
        openSession();
        if (terminal.isNotBusy()) {
            session.save(flight);
            System.out.println("Flight saved with success");
            terminal.isBusy();
        } else {
            flight.setDeparture(flight.getDeparture().plusHours(1));
            session.save(flight);
            System.out.println("We have to reschedule your departure with 1 hour");
        }
        System.out.println("Flight registration failed");
        closeSession();
    }

    public void deleteFlight(Flight flight) {
        openSession();
        session.delete(flight);
        System.out.println("Flight deleted with success");
        closeSession();
    }

    public void updateFlight(Flight flight) {
        openSession();
        session.update(flight);
        System.out.println("Flight updated with success");
        closeSession();
    }

    public List<Customer> findFlightById(int id) {
        openSession();
        Query query = session.createQuery("FROM Flight where id = '" + id + "'");
        List<Customer> result = query.list();
        closeSession();
        return result;
    }

    private static int randomNumber(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}

