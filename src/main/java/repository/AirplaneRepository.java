package repository;

import config.SessionTransaction;
import entity.Airplane;
import org.hibernate.query.Query;

import java.util.List;

public class AirplaneRepository extends SessionTransaction {

    public void createAirplane(Airplane airplane) {
        openSession();
        session.save(airplane);
        System.out.println("Airplane saved with success");
        closeSession();
    }

    public void deleteAirplane(Airplane plane) {
        openSession();
        session.delete(plane);
        System.out.println("Airplane deleted with success");
        closeSession();
    }

    public void updateAirplane(Airplane plane){
        openSession();
        session.update(plane);
        System.out.println("Airplane updated with success");
        closeSession();
    }

    public List<Airplane> findAirplanesById(int id) {
        openSession();
        Query query = session.createQuery("FROM Airplane where id = '" + id + "'");
        List<Airplane> result = query.list();
        closeSession();
        return result;
    }



}
