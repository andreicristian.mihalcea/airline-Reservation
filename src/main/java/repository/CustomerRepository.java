package repository;

import config.SessionTransaction;
import entity.Customer;
import org.hibernate.query.Query;

import java.util.List;

public class CustomerRepository extends SessionTransaction {
    public void createCustomer(Customer customer) {
        openSession();
        session.save(customer);
        System.out.println("Customer saved with success");
        closeSession();
    }

    public void deleteCustomer(Customer customer) {
        openSession();
        session.delete(customer);
        System.out.println("Customer deleted with success");
        closeSession();
    }

    public void updateCustomer(Customer customer){
        openSession();
        session.update(customer);
        System.out.println("Customer updated with success");
        closeSession();
    }

    public List<Customer> findCustomerById(int id) {
        openSession();
        Query query = session.createQuery("FROM Customer where id = '" + id + "'");
        List<Customer> result = query.list();
        closeSession();
        return result;
    }
}
