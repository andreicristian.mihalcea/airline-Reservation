package repository;

import config.SessionTransaction;
import entity.Airport;
import entity.Customer;
import org.hibernate.query.Query;

import java.util.List;

public class AirportRepository extends SessionTransaction {
    public void createAirport(Airport airport) {
        openSession();
        session.save(airport);
        System.out.println("Airport saved with success");
        closeSession();
    }

    public void deleteAirport(Airport airport) {
        openSession();
        session.delete(airport);
        System.out.println("Airport deleted with success");
        closeSession();
    }

    public void updateAirport(Airport airport){
        openSession();
        session.update(airport);
        System.out.println("Airport updated with success");
        closeSession();
    }

    public List<Customer> findAirportById(int id) {
        openSession();
        Query query = session.createQuery("FROM Airport where id = '" + id + "'");
        List<Customer> result = query.list();
        closeSession();
        return result;
    }
}
