package entity;

import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Data
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    private int ticketNumber;

    private String seatNumber;

    @ManyToOne
    @JoinColumn(name = "terminal_id")
    private Terminal terminal;

    @ManyToOne
    @JoinColumn(name = "airport_id")
    private Airport airport;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "flight_details")
    private Flight flight;

    private int gateNumber;
    private int price;

    public Ticket(String seatNumber, Terminal terminal, Airport airport, Customer customer,
                  Flight flight) {
        this.ticketNumber = randomNumberForSeatNumber(0,terminal.getAirplane().getType().getNumberOfSeats());
        this.seatNumber = seatNumber;
        this.terminal = terminal;
        this.airport = airport;
        this.customer = customer;
        this.flight = flight;
        this.gateNumber = terminal.getGateNumber();
        this.price = flight.getPrice();
    }

    private int randomNumberForSeatNumber(int min, int max) {
        return min + (int)(Math.random() * ((max - min) + 1));
    }
}
