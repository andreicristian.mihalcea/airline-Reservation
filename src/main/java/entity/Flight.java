package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    private int flightNumber;


    private int numberOfSeats;

    @ManyToOne
    @JoinColumn(name = "airport_id")
    private Airport airport;

    @OneToOne
    @JoinColumn(name = "airplane_id")
    private Airplane airplane;

    @OneToMany(mappedBy = "flight")
    private List<Ticket> tickets;

    @OneToMany(mappedBy = "flight")
    private List<DepartureDetails> departureDetails;

    @OneToOne
    private Terminal terminal;

    @Basic
    private LocalDateTime departure;

    @Basic
    private LocalDateTime arrival;

    private int price;
//    @ManyToOne
//    @JoinColumn(name = "departure_date_time")
//    private DepartureDetails departureDetails;

    public Flight(int flightNumber,  Airport airport, Airplane airplane,Terminal terminal,
                  DepartureDetails departureDetails) {
        this.flightNumber = flightNumber;
        this.numberOfSeats = airplane.getType().getNumberOfSeats();
        this.airport = airport;
        this.airplane = airplane;
        this.terminal = terminal;
        this.tickets = new ArrayList<>();
        this.departure = departureDetails.getDeparture();
        this.arrival = departureDetails.getArrival();
        this.price = departureDetails.getPrice();

    }


//    private int randomNumberForFlightNumber(int min, int max) {
//        return min + (int)(Math.random() * ((max - min) + 1));
//    }


}
