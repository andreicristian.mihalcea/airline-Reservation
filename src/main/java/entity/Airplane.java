package entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import support.AirplaneCompany;
import support.AirplaneType;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class Airplane {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column
    @Enumerated(EnumType.STRING)
    private AirplaneType type;

    @Enumerated
    @JoinColumn(name = "airplane_company_id")
    private AirplaneCompany airplaneCompany;


    public Airplane(  AirplaneType type,AirplaneCompany airplaneCompany) {
        this.type = type;
        this.airplaneCompany = airplaneCompany;
    }
}
