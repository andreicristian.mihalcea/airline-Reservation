package entity;

import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class Terminal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    private int gateNumber;

    private boolean busy;

    @OneToOne
    @JoinColumn(name = "flight_id")
    private Flight flight;

    @ManyToOne
    @JoinColumn(name = "airplane_id")
    private Airplane airplane;

    public Terminal(int gateNumber, Airplane airplane) {
        this.gateNumber = gateNumber;
        this.airplane = airplane;
        this.busy = false;
    }

    public boolean isNotBusy() {
        this.busy = true;
        return busy;
    }

    public boolean isBusy() {
        this.busy = false;
        return busy;

    }


}
