package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class Airport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    private String nameOfAirport;

    private String location;

    public Airport(String nameOfAirport, String location) {
        this.nameOfAirport = nameOfAirport;
        this.location = location;
    }
}
