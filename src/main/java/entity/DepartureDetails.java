package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class DepartureDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    private LocalDateTime departure;

    @Basic
    private LocalDateTime arrival;

    private int price;

    @ManyToOne
    @JoinColumn(name = "flight_id")
    private Flight flight;

    public DepartureDetails(LocalDateTime departure, int price) {
        this.departure = departure;
        this.arrival = departure.plusHours(3).plusMinutes(12);
        this.price = price;

    }

}
