package config;

import entity.*;
import org.hibernate.query.Query;
import repository.*;
import support.AirplaneCompany;
import support.AirplaneType;
import support.RandomString;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class SystemSetup {
    public static void startSystem() {

        // Airplane Setup
        Airplane airplane1 = new Airplane(AirplaneType.A310, AirplaneCompany.BLUEAIR);
        Airplane airplane2 = new Airplane(AirplaneType.A300, AirplaneCompany.TAROM);
        Airplane airplane3 = new Airplane(AirplaneType.BOEING747, AirplaneCompany.WIZZAIR);

        AirplaneRepository airplaneRepository = new AirplaneRepository();

        airplaneRepository.createAirplane(airplane1);
        airplaneRepository.createAirplane(airplane2);
        airplaneRepository.createAirplane(airplane3);

        // Airport Setup
        Airport airport = new Airport("Henri Coanda", "Bucharest");

        AirportRepository airportRepository = new AirportRepository();

        airportRepository.createAirport(airport);

        // Customer Setup
        Customer customer = new Customer("Mihalcea", "Andrei");
        Customer customer1 = new Customer("Roman", "Iuliana");
        Customer customer2 = new Customer("Mihalcea", "Roxana");

        CustomerRepository customerRepository = new CustomerRepository();

        customerRepository.createCustomer(customer);
        customerRepository.createCustomer(customer1);
        customerRepository.createCustomer(customer2);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime date = LocalDateTime.now();

        date.format(formatter);

//        DepartureDetails departure = new DepartureDetails(date, 35);
//
//        DepartureDetailsRepository departureDetailsRepository = new DepartureDetailsRepository();
//
//        departureDetailsRepository.createDepartureDetails(departure);
//
//        for (int i = 0; i < 45; i++) {
//            DepartureDetails dep = new DepartureDetails(date.plusHours(1 + i).plusMinutes(6 + i), 16 + i);
//            departureDetailsRepository.createDepartureDetails(dep);
//        }

        // Terminal Setup
        TerminalRepository terminalRepository = new TerminalRepository();

        Terminal terminal1 = new Terminal(1, airplane1);
        Terminal terminal2 = new Terminal(2, airplane2);
        Terminal terminal3 = new Terminal(3, airplane3);
        terminalRepository.createTerminal(terminal1);
        terminalRepository.createTerminal(terminal2);
        terminalRepository.createTerminal(terminal3);

        DepartureDetails departure = new DepartureDetails(date, 35);

        DepartureDetailsRepository departureDetailsRepository = new DepartureDetailsRepository();

        departureDetailsRepository.createDepartureDetails(departure);

        for (int i = 0; i < 20; i++) {
            DepartureDetails dep = new DepartureDetails(date.plusHours(1 + i).plusMinutes(6 + i), 16 + i);
            departureDetailsRepository.createDepartureDetails(dep);
        }

        FlightRepository flightRepository = new FlightRepository();

        TicketRepository ticketRepository = new TicketRepository();

        for (int j = 1; j <10 ; j++) {
            List<DepartureDetails> list = departureDetailsRepository.findDepartureDetailsById(randomNumber(0,20));
            List<Airplane> airplaneList = airplaneRepository.findAirplanesById(randomNumber(1,3));

            for(DepartureDetails departureDetails :list){

                Flight fl = new Flight(randomNumber(100, 250), airport, airplane1, terminal1,departureDetails);
                flightRepository.createFlight(fl, fl.getTerminal());

                Ticket ticket1 = new Ticket(randomString(4),fl.getTerminal(),fl.getAirport(),customer1,fl);
                ticketRepository.createTicket(ticket1,ticket1.getFlight());
            }
        }


        // Flight Setup
        Flight flight = new Flight(randomNumber(100, 250), airport, airplane1, terminal1, departure);


        flightRepository.createFlight(flight, flight.getTerminal());
        // Ticket Setup
        Ticket ticket = new Ticket(randomString(4), flight.getTerminal(), flight.getAirport(), customer, flight);



        ticketRepository.createTicket(ticket,ticket.getFlight());


    }


    private static int randomNumber(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

    private static String randomString(int n) {
        return RandomString.getAlphaNumericString(n);
    }
//    private static int countPassengers(List<Ticket> tickets){
//        if (tickets.isEmpty()){
//            return 0;
//        }else {
//            return tickets.size();
//        }
//    }


}
