package config;


import entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import support.AirplaneCompany;
import support.AirplaneType;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;
    private static Properties properties = new Properties();

    public static SessionFactory getSessionFactory() {
            properties.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
            properties.put(Environment.URL, "jdbc:mysql://localhost:3306/airline_reservation");
            properties.put(Environment.USER, "user");
            properties.put(Environment.PASS, "pw");

            properties.put(Environment.HBM2DDL_AUTO, "update");
            properties.put(Environment.JDBC_TIME_ZONE,"UTC");

            properties.put(Environment.SHOW_SQL, true);
            properties.put(Environment.FORMAT_SQL, true);
            properties.put(Environment.USE_SQL_COMMENTS, true);

            properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
            properties.put(Environment.POOL_SIZE, 5);
            try{
            Configuration configuration = new Configuration();
                configuration.setProperties(properties);
                configuration.addAnnotatedClass(Airplane.class);
                configuration.addAnnotatedClass(Airport.class);
                configuration.addAnnotatedClass(Customer.class);
                configuration.addAnnotatedClass(DepartureDetails.class);
                configuration.addAnnotatedClass(Flight.class);
                configuration.addAnnotatedClass(Terminal.class);
                configuration.addAnnotatedClass(Ticket.class);
                configuration.addAnnotatedClass(AirplaneCompany.class);
                configuration.addAnnotatedClass(AirplaneType.class);



            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .build();

            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
            throw new ExceptionInInitializerError(ex);
        }

        return sessionFactory;
    }

}
